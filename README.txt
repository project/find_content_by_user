***********
* README: *
***********

DESCRIPTION:
------------
This module is en extension-module for find_content. It provides a way
for users to find content by username. It can combine the search by
title and user in the same query.
The code in this module is based on the code of find_content.

INSTALLATION:
-------------
1. Place the entire find_content_by_user directory into your Drupal sites/all/modules/
   directory.

2. Check if find_content module is enabled. If not, enable by navigating to:
    administer > modules

3. Enable the find_content_by_user module by navigating to:
    administer > modules


Author:
-------
Stijn Stroobants (https://www.drupal.org/u/stijnstroobants)
stijn@stijnstroobants.com
